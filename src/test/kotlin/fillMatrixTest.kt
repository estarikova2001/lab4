import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FillMatrix {

    @Test
    fun loopWhile10() {
        var i = 0
        var j = 0
        loopWhile({ i < 10 }) {
            i++
            j++
        }
        assertEquals(j, 10)
    }

    @Test
    fun loopFor10() {
        var i = 0
        loopFor(10) {
            i++
        }
        assertEquals(i, 10)
    }

    @Test
    fun fillMatrix1x1() {
        val m = Matrix<Int>(1, 0)
        fillMatrix(m)
        assertEquals(1, m[0, 0])
    }

    @Test
    fun fillMatrix2x2() {
        val m = Matrix<Int>(2, 0)
        fillMatrix(m)
        assertEquals(4, m[0, 0])
        assertEquals(3, m[0, 1])
        assertEquals(2, m[1, 0])
        assertEquals(1, m[1, 1])
    }

    @Test
    fun fillMatrix3x3() {
        val m = Matrix<Int>(3, 0)
        fillMatrix(m)
        assertEquals(8, m[0, 0])
        assertEquals(7, m[0, 1])
        assertEquals(6, m[0, 2])
        assertEquals(9, m[1, 0])
        assertEquals(5, m[1, 1])
        assertEquals(1, m[1, 2])
        assertEquals(4, m[2, 0])
        assertEquals(3, m[2, 1])
        assertEquals(2, m[2, 2])
    }
}