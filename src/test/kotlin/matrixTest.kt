import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MatrixTest {

    @Test
    fun getSetElem() {
        val m = Matrix<Int>(3, 0)
        m[1, 1] = 400
        assertEquals(m[1, 1], 400)
        m[1, 1] = 6
        assertNotEquals(m[1, 1], 400)
        assertThrows<IllegalArgumentException> {
            m[-1, 0] = 0
        }
        assertThrows<IllegalArgumentException> {
            m[0, 888] = 0
        }
        assertThrows<IllegalArgumentException> {
            m[0, 0] = m[-1, 0]!!
        }
        assertThrows<IllegalArgumentException> {
            m[0, 0] = m[0, 888]!!
        }
    }

    @Test
    fun getSize() {
        val size = 5
        val m = Matrix<Int>(size, 0)
        assertEquals(m.size, size)
    }

    @Test
    fun negativeSize() {
        assertThrows<IllegalArgumentException> {
            val m = Matrix<Int>(-1, 0)
        }
    }
}