class P(val x: Int, val y: Int) {

    operator fun plus(value: P): P {
        return P(this.x + value.x, this.y + value.y)
    }

    override operator fun equals(other: Any?): Boolean {
        return if (other !is P) false else this.x == other.x && this.y == other.y
    }

    operator fun plus(direction: Direction): P {
        val dp = direction.p()
        return P(this.x + dp.x, this.y + dp.y)
    }

    override fun toString(): String {
        return "($x, $y)"
    }
}

private val pNE = P(1, -1)
private val pS = P(0, 1)
private val pW = P(-1, 0)

enum class Direction {
    NE {
        override fun turn() = S
        override fun p() = pNE
    },
    S {
        override fun turn() = W
        override fun p() = pS
    },
    W {
        override fun turn() = NE
        override fun p() = pW
    };

    abstract fun turn(): Direction
    abstract fun p(): P
}
