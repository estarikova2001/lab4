/**
 * целочисленная квадратная матрица
 * @property size размер матрицы
 * @constructor создает матрицу, заполненную нулями
 */
class Matrix<T>(val size: Int, private val initial_value: T) {
    init {
        if (size <= 0 )
            throw IllegalArgumentException("size cant be $size")
    }

    private val cells = Array<Any?>(size * size) { initial_value } as Array<T>

    operator fun get(row: Int, col: Int): T? {
        if (!(row in 0 until size && col in 0 until size))
            throw IllegalArgumentException("cell [$row, $col] is out of borders for matrix[$size, $size]")
        return cells[size * row + col]
    }

    operator fun set(row: Int, col: Int, value: T) {
        if (!(row in 0 until size && col in 0 until size))
            throw IllegalArgumentException("cell [$row, $col] is out of borders for matrix[$size, $size]")
        cells[size * row + col] = value
    }

    operator fun contains(p: P) = (p.y in 0 until size) && (p.x in 0 until size)
}
