
/**
 * цикл while
 * @param criteria критерий продолжения работы цикла
 * @param body тело цикла
 */
fun loopWhile(criteria: () -> Boolean, body: () -> Unit) {
    fun step() {
        if (criteria()) {
            body()
            step()
        }
    }
    step()
}

/**
 * цикл for
 * @param count количество итераций
 * @param body тело цикла
 */
fun loopFor(count: Int, body: () -> Unit) {
    var i = 0
    fun step() {
        if (i < count) {
            body()
            i++
            step()
        }
    }
    step()
}

/**
 * заполняет матрицу
 * @param m матрица
 */
fun fillMatrix(m: Matrix<Int>) {
    val n = m.size

    val t = (n - 1) / 3
    val o = (n - 1) - t * 3
    val col = t * 2 + o
    val row = n - (t + if (o > 1) 1 else 0) - 1
    var p = P(col, row)
    var dr = when (o) {
        0 -> Direction.NE
        1 -> Direction.W
        2 -> Direction.S
        else -> throw RuntimeException("something went wrong")
    }

    val b = Matrix<Boolean>(m.size, false)

    fun set(p: P, value: Int) {
        m[p.y, p.x] = value
        b[p.y, p.x] = true
    }

    var value = 1
    set(p, value)
    if (n == 1) return

    fun able(p: P): Boolean {
        return p in m && !b[p.y, p.x]!!
    }

    while (true) {
        p += dr
        set(p, ++value)
        var nextDr = dr.turn()
        val nextP = p + nextDr
        if (able(nextP)) {
            dr = nextDr
        }
        else {
            if (!able(p + dr)) {
                nextDr = nextDr.turn()
                if (able(p + nextDr)) {
                    dr = nextDr
                }
                else {
                    break
                }
            }
        }
    }
}

fun printMatrix(m: Matrix<Int>) {
    for (row in 0 until m.size) {
        for (col in 0 until m.size) {
            print("${m[row, col]}\t")
        }
        println()
    }
}

fun main() {
    val m = Matrix<Int>(1, 0)
    fillMatrix(m)
    printMatrix(m)
}
